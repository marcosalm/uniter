#!/usr/bin/env node

/*
 * Uniter - JavaScript PHP interpreter
 * Copyright 2013 Dan Phillimore (asmblah)
 * http://asmblah.github.com/uniter/
 *
 * Released under the MIT license
 * https://github.com/asmblah/uniter/raw/master/MIT-LICENSE.txt
 */

/*global process, require */
(function () {
    'use strict';

    var modular = require('modular-amd'),
        optionsManager = require('node-getopt').create([
            ['d', 'dump-ast', 'Dump AST of PHP code instead of executing it'],
            ['r', 'run=<code>', 'Run PHP <code> without using script tags <? ... ?>']
        ]),
        parsedOptions = optionsManager.parseSystem();

    // FIXME!! (In Modular)
    modular.configure({
        paths: {
            'Modular': '/node_modules/modular-amd'
        }
    });

    modular.require([
        'uniter'
    ], function (
        uniter
    ) {
        var hasOwn = {}.hasOwnProperty,
            phpCode,
            phpEngine,
            phpParser;

        function output() {
            process.stdout.write(phpEngine.getStdout().readAll());
            process.stderr.write(phpEngine.getStderr().readAll());
        }

        if (hasOwn.call(parsedOptions.options, 'run')) {
            phpCode = '<?php ' + parsedOptions.options.run;

            // Only dump the AST to stdout if requested
            if (hasOwn.call(parsedOptions.options, 'dump-ast')) {
                phpParser = uniter.createParser('PHP');

                process.stdout.write(JSON.stringify(phpParser.parse(phpCode), null, 4) + '\n');
            } else {
                phpEngine = uniter.createEngine('PHP');

                phpEngine.execute(phpCode).done(function () {
                    output();
                }).fail(function (exception) {
                    output();
                    process.stderr.write(exception.getMessage());
                    process.stdout.write('\n');
                });
            }
        } else {
            optionsManager.showHelp();
            process.exit(1);
        }
    });
}());
